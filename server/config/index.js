// process.env.NODE_ENV = 'production';
const fs = require("fs");
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

var config ={
    env: process.env.NODE_ENV,
    DIR: {
        PUBLIC: 'server/resources/',
        UPLOADS: 'uploads/'
    },
    MYSQL: {
        host: 'localhost',
        user: 'root',
        password: '1111',
        database: 'lecturersconclave'
    },
    ACTIVITY: {
        LOGGED_IN: 1,
        LOGGED_OUR: 2,
        CREATE_USER: 3,
        UPDATE_USER: 4,
        DELETE_USER: 5,
        CREATE_CUSTOMER: 6,
        UPDATE_CUSTOMER: 7,
        DELETE_CUSTOMER: 8,
        NOTIFY_USER: 9,
        RESET_PSW: 10
    },
    FILE_UPLOAD_EXT: ['.obj', 'image/', '.svg'],
    FILE_UPLOAD_ATTR: [
        'preview','doc'
    ],
    FILE_UPLOAD_ACCEC: parseInt("0777", 8),
    port: process.env.PORT || 3007,

    security: {
        secret: "t45g3w45r34tw5ye454uhdgdf",
        expiresIn: "24h"
    },
    MAIL: {
        LOGIN: 'simonpdev@gmail.com',
        PSW: 'simonpass',
    },
    superadmin: {
        simple_pay_gateway_private_key: "test_pr_eb4cd74d71724ab7a6803272690f4f00",
        email: "belloolaseni@gmail.com",
        password: "superpass"
    },
    help: {
        randomStr: function (length) {
            return crypto.createHash('sha1').update(crypto.randomBytes(length || 32)).digest('hex');
        },
        deleteFolderRecursive: function (path, flag) {
            var _self = this;
            if (fs.existsSync(path)) {
                for (var u = 0, files = fs.readdirSync(path); u < files.length; u++) {
                    var file = files[u],
                        curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        _self.deleteFolderRecursive(curPath, true);
                    } else {
                        fs.unlinkSync(curPath);
                    }
                }
                if (flag)fs.rmdirSync(path);
            }
        },
        delteFile:function(path){ fs.unlinkSync(config.DIR.PUBLIC +path);},
        cryptPassword: function (password, callback) {
            bcrypt.genSalt(10, function (err, salt) {
                if (err)
                    return callback(err);

                bcrypt.hash(password, salt, function (err, hash) {
                    return callback(err, hash);
                });
            });
        },
        comparePassword: function (plainPass, hashword, callback) {
            bcrypt.compare(plainPass, hashword, function (err, isPasswordMatch) {
                return err == null ?
                    callback(null, isPasswordMatch) :
                    callback(err);
            });
        },

        saveFile : function (req, next,type) {
            if (req.files) {
                var _file = req.files[type||config.FILE_UPLOAD_ATTR[0]],
                    _dir = config.DIR.PUBLIC;

                if (_file && _file[0]) {
                    _file = _file[0];
                    var buffer = fs.readFileSync(_file.path),
                        _urlFile = config.DIR.PUBLIC + config.DIR.UPLOADS + config.help.randomStr() + _file.originalname,
                        _mainDir;
                    fs.writeFileSync(_urlFile, buffer, 'utf8');
                    fs.unlinkSync(_file.path);
                    next(null, _urlFile.replace(config.DIR.PUBLIC, ''));
                } else {
                    next();
                }
            } else {
                next();
            }


        }
    },
    USER_ROLE: {
        SUPER: 1,
        ADMIN: 2,
        USER: 3
    },
    USER_STATUS: {
        ACTIVE: 0,
        BAN: 1

    }
}
module.exports = config;

