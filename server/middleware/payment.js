const act = require("../models/activity");
const config = require("../config");
const Sequelize = require("sequelize");
const superAgent = require('superagent');
const User = require("../models/user");



function Payment() {
}
Payment.set = function(opt,next){
    var privateKey =  opt.privateKey || config.superadmin.simple_pay_gateway_private_key;
    console.log("****************************",opt);
    superAgent.post('https://checkout.simplepay.ng/v2/payments/card/charge/')
        .set('Content-Type', 'application/json')
        .auth(privateKey, '')
        .send({
            token: opt.sp_token,
            amount: opt.sp_amount,
            amount_currency: 'NGN'
        })
        .end(function (err, res) {
            if (err || !res.ok) {
                if (opt.sp_status === 'true') {
                    // failed charge call, but card processed
                    next(null,res);
                } else {
                    next({message:'can`t upprove payments',err:err,res:res },res);
                }
            } else {  // even is http status code is 200 we still need to check transaction had issues or not
                if (res.body.response_code === 20000) {
                    next(null,res);
                } else {
                    next({message:'can`t upprove payments1',err:err,res:res},res);
                }
            }
        });
}
Payment.withrowFee = function(opt,next){
    var privateKey =  opt.privateKey || config.superadmin.simple_pay_gateway_private_key;
    superAgent.post('https://api.simplepay.ng/v2/payments/recurrent/charge')
        .set('Content-Type', 'application/json')
        .auth(privateKey, '')
        .send(opt)
        .end(function (err, res) {
            if (err || !res.ok) {
                if (opt.sp_status === 'true') {
                    // failed charge call, but card processed
                    next(null,res);
                } else {
                    next({message:'can`t upprove payments' ,opt:opt,err:err,res:res},res);
                }
            } else {  // even is http status code is 200 we still need to check transaction had issues or not
                if (res.body.response_code === 20000) {
                    next(null,res);
                } else {
                    next({message:'can`t upprove payments1'},res);
                }
            }
            
        });
}
module.exports = Payment;