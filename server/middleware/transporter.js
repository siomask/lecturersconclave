var nodemailer = require('nodemailer');
var config = require('../config');
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: config.MAIL.LOGIN, // Your email id
        pass: config.MAIL.PSW // Your password
    }
});
module.exports = transporter;