const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const User = require("./user");

const Activity = sequelize.define('activity', {
    action: {
        type: Sequelize.INTEGER
    },
    changes: {
        type: Sequelize.STRING
    } 
});
Activity.belongsTo(User);
module.exports = Activity;