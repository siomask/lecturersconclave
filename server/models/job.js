const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const User = require("./user");

const Job = sequelize.define('job', {
    title: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    price: {
        type: Sequelize.INTEGER
    },
    details: {
        type: Sequelize.STRING
    },
    preview: {
        type: Sequelize.STRING
    },
    file: {
        type: Sequelize.STRING
    }

});
Job.belongsTo(User);
module.exports = Job;