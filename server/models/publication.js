const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const User = require("./user");

const Publication = sequelize.define('publication', {
    title: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    price: {
        type: Sequelize.INTEGER 
    }, 
    preview: {
        type: Sequelize.STRING 
    }, 
    file: {
        type: Sequelize.STRING 
    } 
 
});
Publication.belongsTo(User);
module.exports = Publication;