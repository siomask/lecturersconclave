const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const User = require("./user");
const Publcation = require("./publication");

const Transactions = sequelize.define('transactions', { 
    price: {
        type: Sequelize.INTEGER
    }  
});
Transactions.belongsTo(User);
Transactions.belongsTo(Publcation);
module.exports = Transactions;