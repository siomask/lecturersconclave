const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");

const User = sequelize.define('persons', {
    firstName: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING
    },
    role: {
        type: Sequelize.INTEGER,
        defaultValue:3
        
    },
    status: {
        type: Sequelize.STRING,
        defaultValue:0
    },
    email:{
        type: Sequelize.STRING,
        unique: 'compositeIndex'
    },
    password:{
        type: Sequelize.STRING
    },
    token:{
        type: Sequelize.STRING
    },
    simple_pay_gateway_private_key:{//SimplePay Gateway private key
        type: Sequelize.STRING
    },
    resetPasswordToken :{
        type: Sequelize.STRING
    }  ,

    birthday:{
        type: Sequelize.STRING
    }, 
    avatar:{
        type: Sequelize.STRING
    },
    cv:{
        type: Sequelize.STRING
    },
    birthCountry:{
        type: Sequelize.STRING
    },
    profpursuit:{
        type: Sequelize.STRING
    },
    marritalStatus:{
        type: Sequelize.STRING
    },
    childrens:{
        type: Sequelize.STRING
    },
    statusInputed:{
        type: Sequelize.STRING
    },
    interest:{
        type: Sequelize.STRING
    },
    research:{
        type: Sequelize.STRING
    },
    profession:{
        type: Sequelize.STRING
    },
    qualification:{
        type: Sequelize.STRING
    },
    publisher:{
        type: Sequelize.STRING
    },
    telephone:{
        type: Sequelize.STRING
    },
    simple_pay_gateway_public_key:{
        type: Sequelize.STRING
    },
    card_id:{
        type: Sequelize.STRING
    }
});
User._exclude =['password','resetPasswordToken','token','card_id'];
User._editable =[
    'status',
    'firstName',
    'lastName',
    'email',
    'adress',
    'telephone',
    'qualification',
    'profession',
    'research',
    'interest',
    'statusInputed',
    'childrens',
    'birthCountry',
    'birthday',
    'marritalStatus',
    'profpursuit',
    'password',
    'simple_pay_gateway_private_key',
    'simple_pay_gateway_public_key'
];
module.exports = User;