const router = require("express").Router();
const config = require("../../../config");

var checkPermission=function(req,res,next){
    if(req.user && req.user.role <= config.USER_ROLE.ADMIN){
        next();
    }else{
        return res.json({
            status: false,
            message: "permission denied"
        });
    }
};
var checkSuperPermission=function(req,res,next){
    if(req.user && req.user.role == config.USER_ROLE.SUPER){
        next();
    }else{
        return res.json({
            status: false,
            message: "permission denied"
        });
    }
}
router.all("/", function (req, res) {
    res.json({
        status: true,
        message: "Info about api",
        data: req.user
    });
});

router.use("/users",checkPermission ,require("./users"));
router.use("/super",checkSuperPermission ,require("./super"));

module.exports = router;
