const router = require("express").Router();
const async = require("async");
const config = require("../../../config");
var fs = require("fs");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const User = require("../../../models/user");
const Activity = require("../../../middleware/activity");

const transporter = require("../../../middleware/transporter");
//get all users
router.get("/", function (req, res) {
    var query = {
        where: {
            id: {
                [Op.notIn]: [req.user.id]
            }
        },
        attributes: {exclude: User._exclude},
        order:[['id','DESC']]
    };
    if (req.user.role == config.USER_ROLE.SUPER) {
        // query.where.role = req.query.role || config.USER_ROLE.ADMIN;
    } else {
        query.where.personId = req.user.id;
    }
    User.findAll(query).then(function (users) {
        res.json({
            status: true,
            data: users,
            message: "Users was successfully get"
        });
    }).catch(function (e) {
        res.json({
            status: false,
            data: e,
            message: "Users was unsuccessfully get"
        });
    })

});
router.post("/notify", function (req, res) {
    var _body = req.body;
    if (!_body.fromEmail || !_body.subject || !_body.message || !(_body.users || _body.customers )) {
        res.json({
            status: false,
            message: "something missing"
        });
    } else {
        var query = {
                where: {
                    id: {
                        [Op.notIn]: [req.user.id]
                    }
                },
                attributes: ['email']
            },
            notify = [config.USER_ROLE.USER];
        if (req.user.role == config.USER_ROLE.SUPER) {
            if (_body.users)notify.push(config.USER_ROLE.ADMIN);

        } else {
            query.where.personId = req.user.id;
        }
        query.where.role = {[Op.in]: notify};

        User.findAll(query).then(function (users) {
            var notifiers = '';
            users.forEach(function (user) {
                if(user && user.email   )notifiers += "," + user.email;
            });
            notifiers =notifiers.substr(1);
            if(_body.recievers)notifiers+=","+_body.recievers;
            var
                mailOptions = {
                    from: _body.fromEmail, // sender address
                    to: notifiers, // list of receivers
                    subject: _body.subject, // Subject line
                    text: _body.message //, // plaintext body
                };

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    Activity.set(req,config.ACTIVITY.NOTIFY_USER);
                    console.log('Message sent: ' + info.response);
                }
               return  res.json({
                    status: true,
                    message: "user(s) was notified"
                });
            });
        }).catch(function (er) {
            console.log(er);
            res.json({
                status: false,
                message: "something missing"
            });
        })
    }


});
router.post("/activity",function (req, res) {
    Activity.get(req,function (er,ect) {
        if(er){
            return res.json({
                status: false,
                message: "can`t get activity"
            });
        }else{
            return res.json({
                status: true,
                data:ect,
                message: "activity list"
            });
        }
    })
});
//update user
router.put("/", function (req, res) {
    var _user = {},
        _body = req.body;

    for (var i = 0; i < User._editable.length; i++) {
        if (_body[User._editable[i]] != undefined) {
            if (i == User._editable.length - 1) {

            } else {
                _user[User._editable[i]] = _body[User._editable[i]];
            }

        }
    }
    if (!_body.id) {
        res.json({
            status: false,
            message: "Users was unsuccessfully updated"
        });
    } else {
        User.findOne({where:{id: _body.id} }).then(function (userS) {
            if(userS){
                User.update(_user, {
                    where: {
                        id: _body.id
                    }
                }).then(function (user) {
                    console.log(userS);
                    var changes ='',explict=['createdAt','updatedAt','id'];
                    for(var field in _user){
                        if(userS[field] == _body[field] ||explict.indexOf(field)>-1)continue;
                        if(_body[field]||userS[field]  )changes +=userS[field]+"->"+_user[field]+";";
                    }
                    Activity.set(req,config.ACTIVITY.UPDATE_USER,changes);
                    return res.json({
                        status: true,
                        message: "User was updated"
                    });
                }).catch(function (e) {
                    return res.status(200).json({
                        status: false,
                        message: "can`t update user, such email already exist"
                    });
                })
            }else{
                return res.status(200).json({
                    status: false,
                    message: "can`t update user2"
                });
            }
        }).catch(function(er){
            console.log(er);
            return res.status(200).json({
                status: false,
                message: "can`t update user1"
            });
        })
    }
});

//delete user
router.delete("/:id", function (req, res) {
    User.destroy({force: true, where: {id: req.params.id}}).then(function () {
        Activity.set(req,config.ACTIVITY.DELETE_USER);
        return res.json({
            status: true,
            message: "User was droped"
        });
    }).catch(function (e) {
        return res.status(200).json({
            status: false,
            message: "can`t update user"
        });
    })
});

module.exports = router;
