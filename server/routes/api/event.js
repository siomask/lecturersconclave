const router = require("express").Router();
const Payment = require("../../middleware/payment");
const transporter = require("../../middleware/transporter");
const config = require("../../config/index");
const fs = require("fs");

var generateMessage = function (data) {
    return '<style>' +
        '.container{' +
        'width: 420px;' +
        '}' +
        'image{' +
        'width: 100% ;' +
        '}' +
        '</style>' +
        '<div class="container">' +
        '<h2>Event</h2>' +
        '<p><h4>Title:' + data.title + '</h4></p>' +
        (data.details ? '<p><h3>Details:</h3>' + data.details + '</p>' : '') +
        '<p><h3>Description:</h3>' + data.description + '</p>' +
        '</div>';
}
router.post("/create", function (req, res, next) {
    var _body = req.body;
    Payment.set({sp_amount: 500000, sp_token: _body.sp_token}, function (e, payment) {
        if (e) {
            return res.json({
                payment: payment,
                status: false,
                message: e.message
            });
        } else {
            config.help.saveFile(req, function (e, path) {
                var
                    mailOptions = {
                        from: req.user.email, // sender address
                        to: config.superadmin.email, // list of receivers
                        subject: "Event Request", // Subject line
                        text: 'Title: ' + _body.title + ", Description: " + _body.description + (_body.details ? ", Details: " + _body.details : ""), //, // plaintext body
                        html: generateMessage(_body) //, // plaintext body,
                    };
                if (path) {
                    mailOptions.attachments = [{
                        filename: path.split("/")[path.split("/").length - 1],
                        content: fs.createReadStream(config.DIR.PUBLIC + path)
                    }];
                }
                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Message sent: ' + info.response);
                    }
                    if (path)fs.unlinkSync(config.DIR.PUBLIC + path);
                    return res.json({
                        status: !error,
                        info: info,
                        message: error ? error : 'Evebt was send'
                    });
                });
            })
        }
    })
});

module.exports = router;

