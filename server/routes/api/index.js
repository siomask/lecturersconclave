const router = require("express").Router();

router.all("/", function (req, res) {
    res.json({
        status: true,
        message: "Info about api",
        data: req.user
    });
});

router.use("/user", require("./user"));
router.use("/publication", require("./publication"));
router.use("/job", require("./job"));
router.use("/admin", require("./admin"));
router.use("/event", require("./event"));

module.exports = router;
