const router = require("express").Router();
const async = require("async");
const sequelize = require("sequelize");
const config = require("../../config");
const Activity = require("../../middleware/activity");
const Payment = require("../../middleware/payment");
const User = require("../../models/user");
const Transaction = require("../../models/transactions");

const Job = require("../../models/job");
const Sequelize = require("sequelize");


router.get("/", function (req, res) {
    Job.findAll({where: {personId: req.user.id}}).then(function (data) {
        return res.status(200).json({
            status: true,
            message: "Job data",
            data: data
        });
    }).catch(function (er) {
        return res.status(200).json({
            status: false,
            message: "Job list"
        });
    });
});

router.get("/all", function (req, res) {
    Job.findAll({
        include: [{
            model: User,
            where: { personId: Sequelize.col('person.id') },
            order:[['id','DESC']],
            attributes:  [
                'firstName',
                'lastName'
            ],
        }]
    }).then(function (data) {
        return res.status(200).json({
            status: true,
            message: "Job data",
            data: data
        });
    }).catch(function (er) {
        return res.status(200).json({
            status: false,
            message: "Job list"
        });
    });
});
//update publications
router.post("/", function (req, res) {
    var _body = req.body;
    if (_body.title && _body.price && _body.description) {
        var createPost = function () {
            _body.personId = req.user.id;
            Job.create(_body).then(function (data) {
                return res.status(200).json({
                    data: data,
                    status: true,
                    message: "Job was created"
                });
            }).catch(function (e) {
                return res.json({
                    status: false,
                    message: e.message
                });
            });
        }

        config.help.saveFile(req, function (er, path) {
            if (er) {
                return res.json({
                    status: false,
                    message: er.message
                });
            } else {
                if (path)_body.preview = path;
                createPost()
            }
        })
    } else {
        return res.status(200).json({
            status: false,
            message: "can`t save Job"
        });
    }
});
//update publicaion
router.put("/", function (req, res) {
    var _body = req.body;
    if (!_body.id)return res.status(200).json({
        status: false,
        message: "can`t update Job"
    });
    if (!(_body.personId == req.user.id))return res.status(200).json({
        status: false,
        message: "permission denied"
    });

    var updatePost = function () {
        var post = {};
        ['title', 'description', 'price', 'details', 'preview'].forEach(function (el) {
            post[el] = _body[el];
        })
        Job.update(post, {where: {id: _body.id}}).then(function (data) {
            return res.status(200).json({
                data: data,
                status: true,
                message: "Job was updated"
            });
        }).catch(function (e) {
            return res.json({
                status: false,
                message: e.message
            });
        });
    }

    config.help.saveFile(req, function (er, path) {
        if (er) {
            return res.json({
                status: false,
                message: er.message
            });
        } else {
            if (path)_body.preview = path;
            updatePost()
        }
    })

});
//delete post
router.delete("/:id", function (req, res) {
    Job.destroy({force: true, where: {id: req.params.id}}).then(function (data) {
        return res.json({
            status: true,
            message: "Job was droped"
        });
    }).catch(function (e) {
        return res.status(200).json({
            status: false,
            message: "can`t update user"
        });
    })
});
 
module.exports = router;
