const router = require("express").Router();
const async = require("async");
const sequelize = require("sequelize");
const config = require("../../config");
const Activity = require("../../middleware/activity");
const Payment = require("../../middleware/payment");
const User = require("../../models/user");
const Transaction = require("../../models/transactions");

const Publications = require("../../models/publication");
const Sequelize = require("sequelize");


router.get("/", function (req, res) {
    Publications.findAll({where: {personId: req.user.id}}).then(function (data) {
        return res.status(200).json({
            status: true,
            message: "publication data",
            data: data
        });
    }).catch(function (er) {
        return res.status(200).json({
            status: false,
            message: "publication list"
        });
    });
});
router.get("/all", function (req, res) {
    Publications.findAll({
        attributes: { exclude: ['file'] },
        include: [{
            model: User,
            where: { personId: Sequelize.col('person.id') },
            attributes:  [
                'simple_pay_gateway_public_key'
            ],
            order:[['id','DESC']]
        }]
    }).then(function (data) {
        return res.status(200).json({
            status: true,
            message: "publication data",
            data: data
        });
    }).catch(function (er) {
        return res.status(200).json({
            status: false,
            message: "publication list"
        });
    });
});
//update publications
router.post("/", function (req, res) {
    var _body = req.body;
    if (_body.title && _body.price && _body.description) {
        var createPost = function () {
            _body.personId = req.user.id;
            Publications.create(_body).then(function (data) {
                return res.status(200).json({
                    data: data,
                    status: true,
                    message: "publication was created"
                });
            }).catch(function (e) {
                return res.json({
                    status: false,
                    message: e.message
                });
            });
        }

        config.help.saveFile(req, function (er, path) {
            if (er) {
                return res.json({
                    status: false,
                    message: er.message
                });
            } else {
                config.help.saveFile(req, function (er, docPath) {
                    if (path)_body.preview = path;
                    if (docPath)_body.file = docPath;
                    createPost()
                },config.FILE_UPLOAD_ATTR[1]);

            }
        })
    } else {
        return res.status(200).json({
            status: false,
            message: "can`t save publication"
        });
    }
});
//update publicaion
router.put("/", function (req, res) {
    var _body = req.body;
    if (!_body.id)return res.status(200).json({
        status: false,
        message: "can`t update publication"
    });
    if (!(_body.personId == req.user.id))return res.status(200).json({
        status: false,
        message: "permission denied"
    });

    var updatePost = function () {
        var post = {};
        ['title', 'description', 'price', 'preview', 'file'].forEach(function (el) {
            post[el] = _body[el];
        })
        Publications.update(post, {where: {id: _body.id}}).then(function (data) {
            return res.status(200).json({
                data: _body,
                status: true,
                message: "publication was created"
            });
        }).catch(function (e) {
            return res.json({
                status: false,
                message: e.message
            });
        });
    }

    config.help.saveFile(req, function (er, path) {
        if (er) {
            return res.json({
                status: false,
                message: er.message
            });
        } else {
            config.help.saveFile(req, function (er, docPath) {
                if (er) {
                    return res.json({
                        status: false,
                        message: er.message
                    });
                } else{
                    if (path)_body.preview = path;
                    if (docPath)_body.file = docPath;
                    updatePost()
                }

            }, config.FILE_UPLOAD_ATTR[1]);
        }
    })
});
//delete post
router.delete("/:id", function (req, res) {
    Publications.destroy({force: true, where: {id: req.params.id}}).then(function (data) {
        return res.json({
            status: true,
            message: "Publication was droped"
        });
    }).catch(function (e) {
        return res.status(200).json({
            status: false,
            message: "can`t update user"
        });
    })
});

//buy publication
router.post("/pay", function (req, res) {
    var _body = req.body,
        forIset = 0.35,
        feed = 0.65;
    if ( _body.post) {
        User.findOne({where: {id: _body.post.personId}}).then(function (user) {
            if (user) {
                Publications.findOne({where: {id: _body.post.id}}).then(function (publication) {
                    if (publication) {
                        Payment.withrowFee({
                            privateKey: user.simple_pay_gateway_private_key,
                            card: req.user.card_id,
                            amount: Math.ceil(publication.price * forIset),
                            amount_currency:"NGN",
                            description:"Payment fee for publication("+publication.id+") selling",
                            email:user.email
                        }, function (e) {
                            if (e) {
                                return res.json({
                                    status: false,
                                    e: e,
                                    message: e.message
                                });
                            } else {
                                Payment.withrowFee({
                                    card: req.user.card_id,
                                    amount:  Math.floor(publication.price * feed),
                                    amount_currency:"NGN",
                                    description:"Payment fee for publication("+publication.id+") selling",
                                    email:user.email
                            }, function (e) {
                                    if (e) {
                                        return res.json({
                                            e: e,
                                            status: false,
                                            message: e.message
                                        });
                                    } else {
                                        Transaction.create({
                                            price: publication.price,
                                            personId: req.user.id,
                                            publicationId: publication.id,
                                        }).then(function (transaction) {
                                            return res.json({
                                                status: true,
                                                message: "You have suuccessfuly bought the publication("+publication.id+")",
                                                data: transaction
                                            });
                                        }).catch(function (e) {
                                            return res.json({
                                                status: false,
                                                message: e.message
                                            });
                                        })
                                    }
                                })
                            }
                        })
                    } else {
                        return res.status(200).json({
                            status: false,
                            message: "can`t approve the buy 2"
                        });
                    }
                }).catch(function (e) {
                    return res.json({
                        status: false,
                        message: e.message
                    });
                })
            } else {
                return res.status(200).json({
                    status: false,
                    message: "can`t approve the buy 1"
                });
            }

        }).catch(function (e) {
            return res.json({
                status: false,
                message: e.message
            });
        });

    } else {
        return res.status(200).json({
            status: false,
            message: "can`t approve the buy"
        });
    }
});
//get all bought publications
router.get("/store-bought", function (req, res) {
    Transaction.findAll({
        where:{personId:req.user.id},
        include: [{
            model: Publications,
            where: { publicationId: Sequelize.col('publication.id') },
            order:[['id','DESC']]
        }]
    }).then(function (data) {
        return res.status(200).json({
            status: true,
            message: "store-bought data",
            data: data
        });
    }).catch(function (er) {
        return res.status(200).json({
            status: false,
            message: "store-bought list"
        });
    });
});
module.exports = router;
