const router = require("express").Router();
const async = require("async");
const sequelize = require("sequelize");
const config = require("../../config");
const Activity = require("../../middleware/activity");
var fs = require("fs");

const User = require("../../models/user");

var saveFile = function (req, next) {
    if (req.files) {
        var _file = req.files[config.FILE_UPLOAD_ATTR[0]],
            _dir = config.DIR.PUBLIC;


        if (_file && _file[0]) {
            _file = _file[0];
            var buffer = fs.readFileSync(_file.path),
                _urlFile = config.DIR.PUBLIC + config.DIR.UPLOADS + config.help.randomStr() + _file.originalname,
                _mainDir;
            fs.writeFileSync(_urlFile, buffer, 'utf8');
            fs.unlinkSync(_file.path);
            next(null, _urlFile.replace(config.DIR.PUBLIC, ''));
        } else {
            next();
        }
    } else {
        next();
    }

};

router.get("/", function (req, res) {

    // var id = (req.params.id === 'undefined') ? req.user._id : req.params.id;

    var user = req.user;
    user.password = null;
    user.token = null;
    return res.status(200).json({
        status: true,
        message: "Profile data",
        data: user
    });
    /*
     if(user.role == config.USER_ROLE.SUPER){
     User.findAll({
     attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'users']]
     }).then(function (e) {
     return res.status(200).json({
     status: true,
     message: "Profile data",
     counts:e[0].dataValues.users,
     data: user
     });
     }).catch(function (er) {
     console.log(er);
     return res.status(200).json({
     status: true,
     message: "Customer list",
     data: user
     });
     });
     }else{
     return res.status(200).json({
     status: true,
     message: "Customer list",
     data: user
     });
     }*/


});
//update user
router.put("/", function (req, res) {
    var _body = req.body,
        _user = {};
    if (!_body.id) {
        return res.status(200).json({
            status: false,
            message: "can`t update empty user"
        });
    }
    User._editable.forEach(function (field) {
        if (req.body[field] )_user[field] = req.body[field];
    })
    function update() {
        config.help.saveFile(req, function (er, avatarPath) {
                config.help.saveFile(req, function (er, cvPath) {
                    if (avatarPath)  _user.avatar = avatarPath;
                    if (cvPath) _user.cv = cvPath;
                    User.update(_user, {
                        where: {
                            id: _body.id
                        }
                    }).then(function (user) {
                        // Activity.set(req,config.ACTIVITY.UPDATE_USER,_body);
                        if (_user.avatar) {
                            if(req.user.avatar)config.help.delteFile(req.user.avatar);
                            req.user.avatar = _user.avatar;
                        }
                        if (_user.cv) {
                            if(req.user.cv) config.help.delteFile(req.user.cv);
                            req.user.cv = _user.cv;
                        }
                        return res.json({
                            user: _user,
                            status: true,
                            message: "User was updated"
                        });
                    }).catch(function (e) {
                        return res.status(200).json({
                            status: false,
                            message: "can`t update user"
                        });
                    })
                }, config.FILE_UPLOAD_ATTR[1])
            }
        )

    }

    if (_user.password) {
        _user.password = config.help.cryptPassword(_user.password, function (e, hash) {
            if (e) {
                return res.status(200).json({
                    status: false,
                    message: "can`t update user psw"
                });
            } else {
                _user.password = hash;
                update();
            }
        })
    } else {
        update();
    }

})
;


module.exports = router;
