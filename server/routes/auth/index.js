const passport = require("passport");
const router = require("express").Router();
var jwt = require("jwt-simple");
const async = require("async");

var config = require("../../config");

const User = require("../../models/user");
const sequelize = require("../../middleware/mysql");
const transporter = require("../../middleware/transporter");
const Activity = require("../../middleware/activity");
const Payment = require("../../middleware/payment");


router.all("/", function (req, res, next) {
    res.json({
        status: true,
        message: "Info about auth."
    });
});

router.post("/register", function (req, res, next) {
    var _dsa,
        passportCtrl = function () {
            passport.authenticate("local", function (err, user, info) {
                if (err) {
                    return res.json({
                        status: false,
                        data: err,
                        message: info.message || "Email or password is incorrect."
                    });
                } else {
                    if (!user) {
                        return res.json({
                            status: false,
                            message: info.message || "Email or password is incorrect."
                        });
                    }

                    req.login(user, function (err) {
                        if (err) {
                            throw err;
                        }

                        res.json({
                            _dsa: _dsa,
                            status: true,
                            message: "Login success.",
                            token: user.token
                        });
                    });
                }


            })(req, res, next);
        };

    var _body = req.body;
    if (!_body || !_body.sp_token || !_body.email || !_body.password) {
        return res.json({
            status: false,
            message: "something missing"
        });
    } else {
        Payment.set({sp_amount: 100000, sp_token: _body.sp_token}, function (e, payment) {
            if (e) {
                return res.json({
                    payment: payment,
                    status: false,
                    message: e.message
                });
            } else {
                var _user = _body,
                    psw = _user.password;
                config.help.cryptPassword(psw, function (er, hash) {
                    if (er) {
                        return res.json({
                            status: false,
                            message: "can`t register 2"
                        });
                    } else {
                        _user.password = hash;
                        _user.card_id = JSON.parse(payment.text).source.id;

                        config.help.saveFile(req, function (er, avatarPath) {
                            config.help.saveFile(req, function (er, cvPath) {
                                if (avatarPath)  _user.avatar = avatarPath;
                                if (cvPath) _user.cv = cvPath;
                                User.create(_user).then(function (user) {
                                    _user.password = psw;
                                    var mailOptions = {
                                        from: config.MAIL.LOGIN, // sender address
                                        to: _user.email, // list of receivers
                                        subject: 'Registration', // Subject line
                                        text: 'Welcome to Lectures Conclave', //, // plaintext body
                                        html: '<b>Welcome to Lectures Conclave ✔</b>' // You can choose to send an HTML body instead
                                    };
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            console.log('Message sent: ' + info.response);
                                        }
                                        req.body = _user;
                                        passportCtrl();
                                    });
                                }).catch(function (er) {
                                    return res.json({
                                        status: false,
                                        message: "Such user already exist"
                                    });
                                });
                            },config.FILE_UPLOAD_ATTR[1])
                        })


                    }
                });
            }
        })
    }
});

router.post("/login", function (req, res, next) {
    passport.authenticate("local", function (err, user, info) {
        if (err) {
            return res.json({
                status: false,
                unlogout: true,
                data: err,
                message: info.message || "Email or password is incorrect."
            });
        } else {
            if (!user) {
                return res.json({
                    status: false, unlogout: true,
                    message: info.message || "Email or password is incorrect."
                });
            }

            req.login(user, function (err) {
                if (err) {
                    return res.json({
                        status: false, unlogout: true,
                        message: err.message || "Email or password is incorrect."
                    });
                }
                Activity.set(req, config.ACTIVITY.LOGGED_IN);
                res.json({
                    status: true,
                    message: "Login success.",
                    token: user.token
                });
            });
        }


    })(req, res, next);

});

router.post("/logout", function (req, res) {
    if (!req.isAuthenticated()) {
        return res.json({
            status: false, unlogout: true,
            message: "You are not logged in."
        });
    }

    req.user.token = "";

    req.user.save().then(function () {
        Activity.set(req, config.ACTIVITY.LOGGED_OUR);
        req.logout();
        req.session.destroy();
        res.json({
            status: true,
            message: "Logout success."
        });
    }).catch(function (e) {
        console.log(e);
        res.json({
            status: false,
            unlogout: true,
            data: e,
            message: "Logout unsuccess."
        });
    });
});
router.get("/resetpassword", function (req, res) {
    try {
        var resetD = (new Buffer(req.url.split("?")[1], 'base64').toString().split("&"));
        User.findOne({
            where: {email: resetD[1].split("=")[1], resetPasswordToken: resetD[0].split("=")[1]}
        }).then(function (user) {
            if (!user) {
                return res.status(200).json({
                    status: false,
                    message: "can`t reset psw for this user"
                });
            } else {
                config.help.cryptPassword(resetD[2].split("=")[1], function (er, hash) {
                    if (er) {
                        return res.json({
                            status: false,
                            message: "can`t register 2"
                        });
                    } else {

                        user.password = hash;
                        user.resetPasswordToken = null;
                        user.save().then(function () {
                            // res.sendFile(path.join(__dirname, "../dist/index.html"));
                            // Activity.set(req,config.ACTIVITY.RESET_PSW);
                            res.redirect('/');
                        }).catch(function (er) {
                            return res.json({
                                status: false,
                                message: "can`t reset psw 2"
                            });
                        })
                    }
                });
            }
        }).catch(function (e) {

        })
    } catch (e) {
        return res.status(200).json({
            status: false,
            message: "No such user"
        });
    }


})
router.post("/resetPsw", function (req, res) {
    var _email = req.body.email;
    if (_email) {
        User.findOne({
            where: {email: _email}
        }).then(function (user) {
            if (!user) {
                return res.status(200).json({
                    status: false,
                    message: "No such user"
                });
            } else {
                user.resetPasswordToken = Date.now();
                user.save().then(function (us) {

                    var newPsw = (Date.now()).toString(32),
                        hasTohash = 'token=' + user.resetPasswordToken + "&email=" + _email + "&psw=" + newPsw,
                        link = ('http://' + req.headers.host + '/auth/resetpassword?' + new Buffer(hasTohash).toString('base64'));

                    console.log(hasTohash);
                    var mailOptions = {
                        to: _email,
                        from: config.MAIL.LOGIN,
                        subject: 'Password Reset',
                        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                        link + '\n\n' +
                        'If you did not request this, please ignore this email and your password will remain unchanged.\n',
                        html: '<h1>Hello ' + user.firstName + '</h1>' +
                        '<p>Please click on the following <a href="' + link + '">link</a> to apply the password <b>"' + newPsw + '"</b></p>'
                    };
                    transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            console.log(error);
                            return res.status(200).json({
                                status: false,
                                message: "can`t inform via email"
                            });
                        } else {
                            console.log('Message sent: ' + info.response);
                            return res.status(200).json({
                                status: true,
                                message: "New temporary password have been send. Pleas view the mail to apply new one"
                            });
                        }
                        ;

                    });
                }).catch(function (er) {
                    console.log(er);
                    return res.status(200).json({
                        status: false,
                        message: "can`t reset psw"
                    });
                })
            }

        }).catch(function (er) {
            console.log(er);
            return res.status(200).json({
                status: false,
                message: "No such user"
            });
        });
    } else {
        res.json({
            status: false,
            message: "missed the email"
        });
    }
});

module.exports = router;
