const path = require("path");
const router = require("express").Router();
const sequelize = require("../middleware/mysql");
const authenticate = require("../middleware/authenticate");
const config = require("../config");

const User = require("../models/user");
const Activity = require("../models/activity");
const Job = require("../models/job");
const Publication = require("../models/publication");
const Transacion = require("../models/transactions");

router.use("/api", authenticate, require("./api"));
router.use("/auth", require("./auth"));
router.use("/public", require("./public"));

//---only at start app
router.get("/init", function (req, res) {
    sequelize
        .authenticate()
        .then(function () {
 
            User.sync({}).then(function (e) {
                Job.sync({}).then().catch(function (e) {console.log("WRONG CREATE Job",e);});
                Activity.sync({}).then().catch(function (e) {console.log("WRONG CREATE Activity",e);});
                Publication.sync({}).then().catch(function (e) {console.log("WRONG CREATE Customer",e);});
                Transacion.sync({}).then().catch(function (e) {console.log("WRONG CREATE Transacion",e);});
                Job.sync({}).then().catch(function (e) {console.log("WRONG CREATE Job",e);});
                config.help.cryptPassword(config.superadmin.password, function (er, hash) {
                    if (er) {
                        return res.json({
                            status: false,
                            message: "can`t register 2"
                        });
                    } else {
                        var _user ={};
                        _user.email = config.superadmin.email;
                        _user.firstName = "Super";
                        _user.lastName = "User";
                        _user.role = config.USER_ROLE.SUPER;
                        _user.password = hash;
                        User.create(_user).then(function (user) {
                            return res.json({
                                status: true,
                                message: "user table was   created"
                            });
                        }).catch(function (e) {
                            return res.json({
                                status: false,
                                message: "user table was bot created"
                            });
                        });
                    }
                });

            }).catch(function (er) {
                console.log(er);
                return res.json({
                    status: false,
                    message: "can`t register user table"
                });
            })
        }).catch(function (er) {
        console.log(er);
        return res.json({
            status: false,
            message: "can`t register user table"
        });
    })
});

module.exports = router;
