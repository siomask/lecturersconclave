const router = require("express").Router();
const config = require("../../config");
const Payment = require("../../middleware/payment");
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: config.MAIL.LOGIN, // Your email id
        pass: config.MAIL.PSW // Your password
    }
});

module.exports = transporter;

router.get("/check", function (req, res) {
    Payment.sendToOwner({}, res)
});

router.post("/sendMail", function (req, res) {
    transporter.sendMail(req.body, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Message sent: ' + info.response);
        }
        res.json({error: error, responce: info})
    });
});


module.exports = router;
