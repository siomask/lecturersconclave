﻿import {Input, Component, OnInit, TemplateRef} from '@angular/core';
import {User} from '../../_models/index';
import {AdminService, ConfigService,UserService} from '../../_services/index';

declare var saveAs:any;

@Component({
    moduleId: module.id,
    selector: 'app-action-user',
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class UserModal implements OnInit {
    @Input() model:User;
    private modal:boolean = false;
    private loading:boolean = false;

    constructor(private adminService:AdminService, private confService:ConfigService,private userService: UserService) {
    }

     

    ngOnInit() {

    }

    toggle() {
        this.model = new User();
        this.modal =!this.modal;
    }


    private actionUser() {
        this.loading = true;
        if (this.model.id) {
            this.adminService.update(this.model)
                .subscribe(
                    data => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                    });
        } else {
            this.adminService.create(this.model)
                .subscribe(
                    data => {

                        if (data.status) {
                            this.modal = false;
                            if(data.data.role == this.confService.USER_ROLE.ADMIN){
                                this.adminService.users.push(data.data);
                            }else{
                                this.adminService.customers.push(data.data);
                            }
                            
                        }
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                    });
        }

    }


}