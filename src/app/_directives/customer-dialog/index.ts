﻿import {Component, Inject,ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormGroup,FormBuilder,FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

@Component({
    selector: 'dialog-overview-user-dialog',
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})
export class CustomerDialog {
    private loading: boolean=false;
    private userForm: FormGroup;
    @ViewChild("formSubmit")
    formSubmit:any;
    constructor(
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<CustomerDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) {

        let pswIsreq =  this.data.model.id?[]:[Validators.required];

        this.userForm = this.formBuilder.group({
            telephone:new FormControl('', []),
            country:new FormControl('', []),
            adress:new FormControl('', []),
            city:new FormControl('', []),
            firstName:new FormControl('', [Validators.required]),
            lastName:new FormControl('', [ Validators.required ])
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSuccess(){
        this.formSubmit['nativeElement'].click();
        this.userForm['submitted'] = true;
        if(!this.userForm.valid)return;
        this.loading = true;
        this.data.onSuccess(this.data.model,()=>{
            this.loading = false;
            this.dialogRef.close();
        });
    }


}