﻿import {Component, Inject,ViewChild} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'dialog-overview-job-dialog',
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})
export class JobDetailDialog {
    constructor(
        public dialogRef: MatDialogRef<JobDetailDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) {


    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSuccess(){
    }
}