﻿import {Component, Inject, ViewChild, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormGroup, FormBuilder, FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

@Component({
    selector: 'dialog-overview-user-dialog',
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})
export class PublicationDialog implements OnInit {
    private loading:boolean = false;
    private hidePsw:boolean = true;
    private userForm:FormGroup;
    @ViewChild("formSubmit")
    formSubmit:any;
    @ViewChild("filePreview") filePreview:any;
    @ViewChild("doc") doc:any;

    constructor(private formBuilder:FormBuilder,
                public dialogRef:MatDialogRef<PublicationDialog>,
                @Inject(MAT_DIALOG_DATA) public data:any) {


        let _form:any ={
            title: new FormControl('', [
                Validators.required
            ]),
            description: new FormControl('', [Validators.required]),
            price: new FormControl('', [Validators.required]),
            preview: new FormControl('', [])
        }
        if(data.isJob)_form.details=new FormControl('', []);
        this.userForm = this.formBuilder.group(_form);


    }

    ngOnInit() {
        let self = this;
        this.filePreview['nativeElement'].addEventListener('change', function (e) {
            let file = e.target.files[0];
            if (file) {
                let fileReader = new FileReader();
                self.data.model.previewFile = file;
                fileReader.onload = function (ev:any) {
                    self.data.model.preview = ev.currentTarget.result;
                }
                fileReader.readAsDataURL(file);
            }
        })
        setTimeout(()=>{
            if(!this.data.isJob)this.doc['nativeElement'].addEventListener('change', function (e) {
                let file = e.target.files[0];
                if (file) {
                    self.data.model.docFile = file;
                }
            })
        },500)

    }

    onNoClick():void {
        this.dialogRef.close();
    }

    onSuccess() {
        this.formSubmit['nativeElement'].click();
        this.userForm['submitted'] = true;
        if (!this.userForm.valid)return;
        this.loading = true;
        this.data.onSuccess(this.data.model, ()=> {
            this.loading = false;
            this.dialogRef.close();
        });
    }


}