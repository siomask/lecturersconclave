import {Injectable, OnInit} from '@angular/core';
import 'rxjs/add/operator/map'

@Injectable()
export class ConfigService implements OnInit {
    PATTERNS:any = {
        URL: /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
        EMAIL: /\S+@\S+\.\S+/
    };
    USER_ROLE:any = {
        SUPER: 1,
        ADMIN: 2,
        CUSTOMER: 3,
        PA: 4,
    }
    USER_STATUS:any = {
        ACTIVE: 0,
        BAN: 1,
    }
    TOKENS:any ={
        pay_public:'test_pu_f35c4cc5242141e2ade392dcceab855d'
    }
    ACTIVITY:any = [
        "",
        "have beed logged in",
        "have beed logged out",
        "have beed create the user ",
        "have beed update the user ",
        "have beed delete the user ",
        "have beed create customer ",
        "have beed update customer ",
        "have beed delete customer ",
        "have beed send messages",
        "have beed reset password",
    ]

    DEFAUTL:any={
        IMG:{
            AVATAR:'https://upload.wikimedia.org/wikipedia/commons/e/e4/Elliot_Grieveson.png'
        }
    }
    constructor() {
    }

    ngOnInit() {

    }

}