﻿import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {AlertService} from './alert.service';
import {User} from '../_models/index';
import {ConfigService} from "./config";
import 'rxjs/add/operator/map'
import {Router} from '@angular/router';

declare var window:any;

@Injectable()
export class USER_SERV {
    protected REMOTE_API:string = '/api/user';
    USER:any = {};
    customers:any = [];
    users:any = [];

    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService) {
    }

    protected checkToken(_r:any, flag:any = false) {
        if ( _r.unlogout) {
            this.alertService.error(_r.message);
            let _logout = this.auth.logout()
            if (_logout)_logout.subscribe((data)=> {
                console.log(data);
            });
        } else if (!_r.status) {
            this.alertService.error(_r.message);
        } else {
            if (!flag)this.alertService.success(_r.message);
        }
        return true;
    }


    protected jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers();
        if (currentUser && currentUser.token) {
            headers.set('authorization',currentUser.token)
        }
        return new RequestOptions({headers: headers});
    }
}


@Injectable()
export class RequestService extends USER_SERV {
    constructor(protected http:Http, protected alertService:AlertService, protected confService:ConfigService ) {
        super(http, null, alertService);
    }
    request(url:any,data:any){
        let _jwt = this.jwt();
        _jwt.headers.set('Accept', 'application/json');
        let formData:FormData = new FormData(),
            exept = ['avatar', 'cv', 'cvFile', 'imgFile' , 'image' ];
        for (let filed in data) {
            if (exept.indexOf(filed) < 0) formData.append(filed, data[filed] ==null?'':data[filed]);
        }
        if (data.imgFile)formData.append('preview', data.imgFile);
        return this.http.post(url, formData, _jwt).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    
}
@Injectable()
export class AuthenticationService extends USER_SERV {
    constructor(protected http:Http, protected alertService:AlertService, protected confService:ConfigService, private router:Router) {
        super(http, null, alertService);
        this.auth = this;
    }

    login(username:string, password:string) {
        return this.http.post('/auth/login', ({email: username, password: password}))
            .map((response:Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                this.checkToken(user, true);
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            });
    }

    resetPsw(username) {
        return this.http.post('/auth/resetPsw', ({email: username}))
            .map((response:Response) => {
                let _r = response.json();
                this.checkToken(_r);
                return _r;
            });
    }

    logout() {
        if (localStorage.getItem('currentUser')) {
            return this.http.post('/auth/logout', ({}))
                .map((response:Response) => {
                    // login successful if there's a jwt token in the response
                    let user = response.json();
                    this.checkToken(user, true);
                    if (user.status || user.unlogout) {
                        localStorage.removeItem('currentUser');
                        this.router.navigate(['/login']);
                    }

                    return user;
                });
        }
    }
}
@Injectable()
export class UserService extends USER_SERV {

    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService, protected confService:ConfigService) {
        super(http, auth, alertService);
    }

    isAdmin() {
        return this.USER && (this.USER.role == this.confService.USER_ROLE.ADMIN || this.USER.role == this.confService.USER_ROLE.SUPER);
    }

    isPublisher() {
        return this.USER && (this.USER.publisher == 1);
    }

    isSuperAdmin() {
        return this.USER && this.USER.role == this.confService.USER_ROLE.SUPER;
    }

    user(next) {
        if (this.USER) {
            next(this.USER);
        } else {
            this.getOne().subscribe((res)=> {
                next(this.USER);
            })
        }
    }

    getOne() {
        return this.http.get(this.REMOTE_API, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r, true);
            this.USER = _r.data || {};
            this.USER.birthday = new Date(this.USER.birthday);
            if (_r.counts)  this.USER.users = _r.counts - 1;
            return this.USER;
        });
    }

    create(user:any,payment) {
        let _jwt = this.jwt();
        _jwt.headers.set('Accept', 'application/json');
        let formData:FormData = new FormData(),
            exept = ['avatar', 'cv', 'cvFile', 'avatarFile'];
        for (let filed in user) {
            if (exept.indexOf(filed) < 0) formData.append(filed, user[filed] ==null?'':user[filed]);
        }
        for (let filed in payment) {
            if (exept.indexOf(filed) < 0) formData.append(filed, payment[filed] ==null?'':payment[filed]);
        }
        if (user.avatarFile)formData.append('preview', user.avatarFile);
        if (user.cvFile)formData.append('doc', user.cvFile);
        
        return this.http.post('auth/register', formData, _jwt).map((response:Response) => {
            let _r = response.json();
            if (_r && _r.token) {
                localStorage.setItem('currentUser', JSON.stringify(_r));
            }
            return _r;
        });
    }

    update(user:any) {
        let _jwt = this.jwt();
        _jwt.headers.set('Accept', 'application/json');
        let formData:FormData = new FormData(),
            exept = ['avatar', 'cv', 'cvFile', 'avatarFile'];
        for (let filed in user) {
            if (exept.indexOf(filed) < 0) formData.append(filed, user[filed] ==null?'':user[filed]);
        }
        if (user.avatarFile)formData.append('preview', user.avatarFile);
        if (user.cvFile)formData.append('doc', user.cvFile);
        return this.http.put(this.REMOTE_API, formData, _jwt).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            if (_r.status) {
                if (user.cvFile)user.cv = _r.user.cv;
                if (user.avatarFile)user.avatar = _r.user.avatar;
                user.avatarFile = user.cvFile = null;
            }
            return _r;
        });
    }

    delete(id:number) {
        return this.http.delete(this.REMOTE_API + id, this.jwt()).map((response:Response) => response.json());
    }

}

@Injectable()
export class PublicationsService extends USER_SERV {

    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService, protected confService:ConfigService) {
        super(http, auth, alertService);
        this.REMOTE_API = 'api/publication'
    }


    get(url:any = null,_url:any=false) {
        return this.http.get(_url || (this.REMOTE_API + (url ? "/" + url : '')), this.jwt()).map((response:Response) => {
            let _r = response.json();
            return _r;
        });
    }
    getAll(url:any=false) {
        return this.http.get(url||(this.REMOTE_API + "/all"), this.jwt()).map((response:Response) => {
            return response.json();
        });
    }

    create(data:any,url:any=false) {
        let _XHR = window.XMLHttpRequest || window.XDomainRequest || window.ActiveXObject;
        let xhr = new _XHR("Microsoft.XMLHTTP");
        xhr.open('POST', url||this.REMOTE_API, false);
        xhr.setRequestHeader("authorization", JSON.parse(localStorage.getItem('currentUser')).token);
        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                var percentComplete = (e.loaded / e.total) * 100;
                console.log(percentComplete + '% uploaded');
            }
        };
        xhr.send(data);
        return xhr.responseText;
    }

    update(data:any,url:any=false) {
        let _XHR = window.XMLHttpRequest || window.XDomainRequest || window.ActiveXObject;
        let xhr = new _XHR("Microsoft.XMLHTTP");
        xhr.open('PUT', url||this.REMOTE_API, false);
        xhr.setRequestHeader("authorization", JSON.parse(localStorage.getItem('currentUser')).token);
        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                var percentComplete = (e.loaded / e.total) * 100;
            }
        };
        xhr.send(data);
        return xhr.responseText;
    }

    delete(id:number,url:any=false) {
        return this.http.delete(url || this.REMOTE_API + "/" + id, this.jwt()).map((response:Response) => response.json());
    }
    request(url, data:any = {}) {
        return this.http.post(this.REMOTE_API + "/" + url, data, this.jwt()).map((response:Response) => {
            return response.json();
        });
    }

}
@Injectable()
export class AdminService extends USER_SERV {

    CUSTOMER_API:string = '/api/customer';
    ADMIN_API:string = '/api/admin/users';
    SUPER_ADMIN_API:string = '/api/admin/super';

    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService) {
        super(http, auth, alertService);
    }

    create(user:User) {
        return this.req(user);

    }

    createCustomer(data:any) {
        return this.http.post(this.CUSTOMER_API, data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });

    }

    getAll(data) {
        return this.http.get(this.ADMIN_API + (data ? "?role=" + data.role : ''), this.jwt()).map((response:Response) => {
            let _r = response.json();
            // this.checkToken(_r);
            return _r;
        });
    }

    getAllCustomer(data) {
        return this.http.get(this.CUSTOMER_API, this.jwt()).map((response:Response) => {
            let _r = response.json();
            return _r;
        });
    }

    update(user:any) {
        return this.http.put(this.ADMIN_API, user, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    updateCustomer(user:any) {
        return this.http.put(this.CUSTOMER_API, user, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    request(url, data:any = {}) {
        return this.http.post(this.SUPER_ADMIN_API + "/" + url, data, this.jwt()).map((response:Response) => {
            return response.json();
        });
    }

    req(data:any = {}, url:string = null) {
        return this.http.post(this.ADMIN_API + (url ? "/" + url : ""), data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            if (url != 'activity')this.checkToken(_r);
            return _r;
        });
    }

    delete(id:number) {
        return this.http.delete(this.ADMIN_API + "/" + id, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    deleteCustomer(id:number) {
        return this.http.delete(this.CUSTOMER_API + "/" + id, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

}