import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpModule,BaseRequestOptions } from '@angular/http';


import { AppComponent } from './app.component';
import { routing }        from './app.routing';

import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule,MatAutocompleteModule} from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';


import { AlertComponent,UserModal,DialogOverviewExampleDialog,UserDialog,CustomerDialog,PublicationDialog,BannerSample } from './_directives/index';
import { AuthGuard,LoginGuard ,AdminGuard,SuperAdminGuard} from './_guards/index';
import { AlertService, AuthenticationService, UserService,AdminService,ConfigService,PublicationsService,RequestService } from './_services/index';
import { EventsComponent,HomeComponent,UserListComponent ,ProfileComponent,DefaultComponent,PersonalPublicationComponent,StoreBoughtComponent,AllPublicationsComponent,AccountComponent} from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register';
import { FilterNamesPipe,OrderrByPipe } from './_pipes';
import {CdkTableModule} from "@angular/cdk/table";
import {JobComponent} from "./home/account/job/index";
import {AllJobsComponent} from "./home/all.jobs/index";
import {JobDetailDialog} from "./_directives/job.detail/index";

import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,

    AlertComponent,
    HomeComponent,
    ProfileComponent,
    UserListComponent,
    LoginComponent,
    RegisterComponent,
    PersonalPublicationComponent,
    DefaultComponent,
    UserModal,
    DialogOverviewExampleDialog,
    UserDialog,
    CustomerDialog,
    PublicationDialog,
    JobDetailDialog,
    StoreBoughtComponent,
    AccountComponent,
    AllPublicationsComponent,
    EventsComponent,
    JobComponent,
    AllJobsComponent,
    BannerSample,

    FilterNamesPipe,
    OrderrByPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatCardModule,
    routing,

    MatIconModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSortModule,
    MatTableModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatInputModule,
    MatTooltipModule,

      
    NgxCarouselModule,

    CdkTableModule,

  ],
  providers: [
    LoginGuard,
    AdminGuard,
    SuperAdminGuard,
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    AdminService,
    ConfigService,
    PublicationsService,
    RequestService,
  ],
  entryComponents: [DialogOverviewExampleDialog,UserDialog,CustomerDialog,PublicationDialog,JobDetailDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
