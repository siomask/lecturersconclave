﻿import {Routes, RouterModule} from '@angular/router';

import {HomeComponent} from './home/index';
import {ProfileComponent} from './home/profile/index';
import {UserListComponent} from './home/users/index';
import {LoginComponent} from './login/index';
import {RegisterComponent} from './register';
import {AuthGuard, LoginGuard,AdminGuard} from './_guards/index';
import {DefaultComponent} from "./home/default/index";
import {AccountComponent,PersonalPublicationComponent,StoreBoughtComponent,AllPublicationsComponent,EventsComponent} from "./home";
import {JobComponent} from "./home/account/job/index";
import {AllJobsComponent} from "./home/all.jobs/index";

const appRoutes:Routes = [
    {
        path: '', component: HomeComponent, canActivate: [AuthGuard],
        children: [
            {path: '', component: DefaultComponent},
            {path: 'account', component: AccountComponent,children:[
                {path: 'profile', component: ProfileComponent} ,
                {path: 'personalpublications', component: PersonalPublicationComponent} ,
                {path: 'store-bought', component: StoreBoughtComponent},
                {path: 'jobs', component: JobComponent}
            ]},

            {path: 'jobs', component: AllJobsComponent} ,
            {path: 'allpublications', component: AllPublicationsComponent} ,
            {path: 'events', component: EventsComponent} ,
            // {path: 'mail', component: MailComponent, canActivate: [AdminGuard]},
            // {path: 'users', component: UserListComponent, canActivate: [SuperAdminGuard]},
            // {path: 'activity', component: ActivityComponent, canActivate: [AdminGuard]},
            // {path: 'customers', component: CustomerListComponent, canActivate: [AdminGuard]}
        ]
    },

    {path: 'login', component: LoginComponent, canActivate: [LoginGuard]},
    {path: 'register', component: RegisterComponent, canActivate: [LoginGuard]},

    // otherwise redirect to home
    {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes);