﻿import {ViewChild, Component, OnInit, TemplateRef, Inject} from '@angular/core';
import {User} from '../../_models/index';
import {AdminService, ConfigService} from '../../_services/index';
import {Config} from '../../_helpers';
import {UserModal, DialogOverviewExampleDialog, UserDialog} from '../../_directives';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

declare var saveAs:any;


@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class AccountComponent implements OnInit {
   

    constructor(private adminService:AdminService, private confService:ConfigService, private dialog:MatDialog) {
    }

    ngOnInit() {
       
    }

   
}