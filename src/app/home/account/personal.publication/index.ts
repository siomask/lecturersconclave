﻿import {ViewChild, Component, OnInit, TemplateRef, Inject} from '@angular/core';
import {User} from '../../../_models/index';
import {AdminService, ConfigService} from '../../../_services/index';
import {Config} from '../../../_helpers';
import {UserModal, DialogOverviewExampleDialog, PublicationDialog} from '../../../_directives';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {PublicationsService, UserService} from "../../../_services/user.service";

declare var saveAs:any, alertify:any;


@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class PersonalPublicationComponent implements OnInit {
    private isDesc:boolean = false;
    private modal:boolean = false;
    private model:User = new User();
    private column:string = 'CategoryName';
    private data:Array<any> = [];
    private direction:number;
    @ViewChild("userACtion")
    userACtion:UserModal;

    displayedColumns = ['id', 'title', 'description', 'price', 'file', 'createdAt', 'actions'];
    dataSource:MatTableDataSource<any> = new MatTableDataSource([]);
    @ViewChild(MatPaginator) paginator:MatPaginator;
    @ViewChild(MatSort) sort:MatSort;

    constructor(private postService:PublicationsService, private confService:ConfigService, private userService:UserService, private dialog:MatDialog) {
    }

    ngOnInit() {
        this.postService.get().subscribe((data)=> {
            if (data.status) {
                data.data.forEach((el)=> {
                    el['actions'] = 1;
                });
                this.data = data.data;
                this.refreshData();
            }
        });

    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    private refreshData() {
        this.dataSource = new MatTableDataSource(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

    }

    private  openDialog(text, next):void {
        let data:any = {text: text},
            dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
                width: '250px',
                data: data
            });
        dialogRef.afterClosed().subscribe(result => {
            if (data.isOk)next();
        });
    }

    private  openPostDialog(data:any = {}):void {
        let _data:any = {
                text: "Create/Edit new Publication",
                model: data,
                onSuccess: (data, next)=> {
                    this.actionPost(data, next);
                }
            },
            dialogRef = this.dialog.open(PublicationDialog, {
                width: '50%',
                data: _data
            });
    }

    private applyFilter(filterValue:string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    private select(data) {
        this.openPostDialog(data);
    }

    private addNewPublicatioin() {
        if(!this.userService.USER.simple_pay_gateway_private_key )return alertify.error('Add some SimplePay Gateway private  key in your profile')
        this.openPostDialog();
    }

    private actionPost(data, next) {
        var formData = new FormData();
        formData.append('title', data.title);
        formData.append('description', data.description);
        formData.append('price', data.price);
        formData.append('personId', data.personId);
        
        if (data.previewFile)formData.append('preview', data.previewFile);
        if (data.docFile)formData.append('doc', data.docFile);
        let onSucch = (_r)=>{
            if (data.docFile)data.file = _r.data.file;
            if (data.previewFile)data.preview = _r.data.preview;
            data.previewFile = data.docFile = null;
        }
        if (data.id) {
            formData.append('id', data.id);
            let res = JSON.parse(this.postService.update(formData));
            if (res.status) {
                onSucch(res);
            }else{
                alertify.error(res.message);
            }
        } else {
            let res = JSON.parse(this.postService.create(formData));
            if (res.status) {
                onSucch(res);
                this.data.push(res.data);
                this.refreshData();
            }
        }
        next();

    }


    private remove(user) {
        this.openDialog("are you sure to drop " + user.id, ()=> {
            this.postService.delete(user.id).subscribe((data)=> {
                if (data.status) {
                    for (let i = 0; i < this.data.length; i++) {
                        if (this.data[i].id == user.id) {
                            this.data.splice(i, 1);
                            return this.refreshData();
                        }
                    }
                }
            });
        });
    }


    private  sortF(property) {
        this.isDesc = !this.isDesc; //change the direction
        this.column = property;
        this.direction = this.isDesc ? 1 : -1;
    };
}