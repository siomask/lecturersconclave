﻿import {ViewChild, Component, OnInit, TemplateRef} from '@angular/core';
import {AdminService, ConfigService, PublicationsService} from '../../../_services/index';
import {UserService} from "../../../_services/user.service";
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

declare var alertify:any, SimplePay:any;

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class StoreBoughtComponent implements OnInit {
    private data:any = [];
    displayedColumns = ['id', 'publicationId', 'price' , 'createdAt', 'file' ];
    dataSource:MatTableDataSource<any> = new MatTableDataSource([]);
    @ViewChild(MatPaginator) paginator:MatPaginator;
    @ViewChild(MatSort) sort:MatSort;
    constructor(private postService:PublicationsService, private confService:ConfigService, private userService:UserService) {
    }

    ngOnInit() {

        this.postService.get('store-bought').subscribe((data)=>{
            if(data.status){
                this.data = data.data;
                this.refreshData();
            }
        })

    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    private refreshData() {
        this.data.forEach((el)=>{
            el.file = el.publication.file;
        })
        this.dataSource = new MatTableDataSource(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

    }
    private applyFilter(filterValue:string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

}