﻿import {  Component, OnInit } from '@angular/core';
import { PublicationsService} from '../../_services/index';
import {JobDetailDialog} from "../../_directives/job.detail/index";
import { MatDialog} from '@angular/material';

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class AllJobsComponent implements OnInit {
    private data:any = {posts: []};

    constructor(private postService:PublicationsService, private dialog:MatDialog) {
    }

    ngOnInit() {
        this.postService.getAll('api/job/all').subscribe((data)=> {
            if (data.status) {
                this.data.jobs = data.data;
            }
        })

    }
    private  openJobDialog(_data:any = {}):void {
        let data:any = {
                model: _data, onSuccess: (user, next)=> {}
            },
            dialogRef = this.dialog.open(JobDetailDialog, {
                width: '50%',
                data: data
            });
    }

}