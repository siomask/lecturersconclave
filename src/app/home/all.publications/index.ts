﻿import {ViewChild, Component, OnInit, TemplateRef} from '@angular/core';
import {AdminService, ConfigService, PublicationsService} from '../../_services/index';
import {UserService} from "../../_services/user.service";
import {DialogOverviewExampleDialog} from "../../_directives/dialog/index";
import { MatDialog } from '@angular/material';
declare var alertify:any, SimplePay:any;

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class AllPublicationsComponent implements OnInit {
    private data:any = {posts: []};

    constructor(private postService:PublicationsService, private confService:ConfigService, private userService:UserService, private dialog:MatDialog) {
    }

    ngOnInit() {
        this.postService.getAll().subscribe((data)=> {
            if (data.status) {
                this.data.posts = data.data;
            }
        })

    }
    private buy( token, paid,post){
        this.postService.request('pay',{sp_token:token, sp_paid:paid,post:post}).subscribe((res)=>{
            if(res.status){
                
            }else{
                alertify.error(res.message);
            }
        })
    }
    private  openDialog(text, next):void {
        let data:any = {text: text},
            dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
                width: '250px',
                data: data
            });
        dialogRef.afterClosed().subscribe(result => {
            if (data.isOk)next();
        });
    }
    private checkPayment(post){
        if(this.userService.USER.id == post.personId){
            return alertify.error('it seem`s like it`s your post');
        }else{
            this.openDialog("Are you sure you want to buy this publication, it will cost you "+post.price+'(NGN)',()=>{
                this.postService.request('pay',{post:post}).subscribe((res)=>{
                    if(res.status){
                        alertify.success(res.message);
                    }else{
                        alertify.error(res.message);
                    }
                })
            })
        }
        
    }

}