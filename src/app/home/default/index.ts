﻿import {ViewChild, Component, OnInit, TemplateRef} from '@angular/core';
import {AdminService, ConfigService, PublicationsService} from '../../_services/index';
import {UserService} from "../../_services/user.service";
import {DialogOverviewExampleDialog} from "../../_directives/dialog/index";
import { MatDialog } from '@angular/material';
import { NgxCarousel } from 'ngx-carousel';

declare var alertify:any, SimplePay:any;

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class DefaultComponent implements OnInit {
    private data:any = {posts: []};
    public carouselOne: NgxCarousel;
    constructor(private postService:PublicationsService, private confService:ConfigService, private userService:UserService, private dialog:MatDialog) {
    }

    ngOnInit() {
        
    }

    private  openDialog(text, next):void {
        let data:any = {text: text},
            dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
                width: '250px',
                data: data
            });
        dialogRef.afterClosed().subscribe(result => {
            if (data.isOk)next();
        });
    }
   

}