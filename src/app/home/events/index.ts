﻿import {ViewChild, Component, OnInit, TemplateRef, Inject} from '@angular/core';
import {User} from '../../_models/index';
import {AdminService, ConfigService} from '../../_services/index';
import {Config} from '../../_helpers';
import {UserModal, DialogOverviewExampleDialog, UserDialog} from '../../_directives';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {
    FormGroup,
    FormBuilder,
    FormControl,
    FormGroupDirective,
    NgForm,
    Validators,
    ValidatorFn,
    AbstractControl
} from '@angular/forms';
import {RequestService, UserService} from "../../_services/user.service";

declare var SimplePay:any, alertify:any;

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class EventsComponent implements OnInit {
    @ViewChild("image") image:any;
    private loading:boolean = false;
    private eventForm:FormGroup;
    private model:any = {};

    constructor(private reqService:RequestService,
                private userService:UserService,
                private confService:ConfigService,
                private formBuilder:FormBuilder) {
        this.eventForm = this.formBuilder.group({
            title: new FormControl('', [
                Validators.required,
            ]),
            description: new FormControl('', [
                Validators.required,
            ]),
            details: new FormControl('', [])
        });
    }

    ngOnInit() {
        let self = this;
        this.image.nativeElement.addEventListener('change', (e)=> {
            let file = e.target.files[0];
            if (file) {
                let fileReader = new FileReader();
                self.model.imgFile = file;
                fileReader.onload = function (ev:any) {
                    self.model.image = ev.currentTarget.result;
                }
                fileReader.readAsDataURL(file);
            }
        })
    }


    private checkPayment() {
        if (!this.eventForm.valid)return;
        if (this.model.sp_token) {
            this.event(this.model.sp_token, true);
        } else {
            var handler = SimplePay.configure({
                token: (token, paid)=> {
                    this.event(
                        token, paid
                    )
                },
                key: this.confService.TOKENS.pay_public
            });
            handler.open(SimplePay.REMEMBER, {
                email: this.userService.USER.email,
                phone: this.userService.USER.telephone,
                description: 'Payment for event',
                amount: 500000,
                currency: 'NGN'
            });
        }

    }

    private event(token, paid) {
        if (paid) {
            this.loading = true;
            this.model.sp_token = token;
            this.reqService.request('api/event/create', this.model).subscribe((resp)=> {
                this.loading = false;
            })
        } else {
            alertify.error('Paid was failed');
        }

    }
}