﻿import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService, UserService, AdminService} from '../_services/index';
import {ConfigService} from "../_services/config";

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['index.scss']
})

export class HomeComponent implements OnInit {
    private year:any = (new Date()).getFullYear();

    constructor(private authService:AuthenticationService,
                private confService:ConfigService,
                private userService:UserService,
                private adminService:AdminService) {
    }

    ngOnInit() {
        this.userService.getOne().subscribe((data)=> {  });
    }


    private logOut() {
        this.authService.logout().subscribe((data:any)=> {
            console.log(data);
        })
    }
}