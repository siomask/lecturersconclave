﻿import {ViewChild,Component, OnInit} from '@angular/core';

import {User} from '../../_models/index';
import {UserService,ConfigService} from '../../_services/index';
import {FormGroup,FormBuilder,FormControl, FormGroupDirective, NgForm, Validators,ValidatorFn,AbstractControl} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

export function matchPswValidator(model:any): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const forbidden = model.currentUser.password ==model.currentUser.password_repeat ||(!model.currentUser.password && !model.currentUser.password_repeat),
            res = {};
        res['password_match']={value: control.value};
        return !forbidden ? res : null;
    };
}

@Component({
    moduleId: module.id,
    templateUrl: 'profile.html',
    styleUrls: ['index.scss']
})

export class ProfileComponent implements OnInit {
    private currentUser:any={};
    private hidePsw: boolean = true;
    private loading: boolean;
    private userForm: FormGroup;
    @ViewChild("avatar")avatar:any;
    @ViewChild("cv")CV:any;
    private els:any={
        status:[
            'Employed',
            'Self Employed',
            'Jobless and searching for job',
            'Jobless and not searching for job',
            'Others'
        ],
        qualifications:[
            'B.Sc.',
            'B.Sc1.'
        ]
    };
    constructor(  private formBuilder: FormBuilder,private userService:UserService,private confService:ConfigService) {
        this.userForm = this.formBuilder.group({
            email:    new FormControl('', [
                Validators.required,
                Validators.email,
            ]),
            firstName:new FormControl('', [ Validators.required ]),
            lastName:new FormControl('', [ Validators.required ]),
            telephone:    new FormControl('', [
                Validators.required
            ]),
            password:new FormControl('', [
                matchPswValidator(this)
            ]),

            birthday:new FormControl('', []),
            birthCountry:new FormControl('', []),
            profpursuit:new FormControl('', []),
            marritalStatus:new FormControl('', []),
            childrens:new FormControl('', []),
            statusInputed:new FormControl('', []),
            status1:new FormControl('', []),
            interest:new FormControl('', []),
            simple_pay_gateway_private_key:new FormControl('', []),
            research:new FormControl('', []),
            profession:new FormControl('', []),
            qualification:new FormControl('', []) 
        });
    }

    ngOnInit() {
        if(!this.userService.USER.id){
            this.userService.getOne().subscribe((data)=> {
                this.currentUser = this.userService.USER;
            });
        }else{
            this.currentUser = this.userService.USER;
        }

        let self = this;
        this.avatar.nativeElement.addEventListener('change',(e)=>{
                let file = e.target.files[0];
                if (file) {
                    let fileReader = new FileReader();
                    self.currentUser.avatarFile = file;
                    fileReader.onload = function (ev:any) {
                        self.currentUser.avatar = ev.currentTarget.result;
                    }
                    fileReader.readAsDataURL(file);
                }
        })
        this.CV.nativeElement.addEventListener('change',(e)=>{
                let file = e.target.files[0];
                if (file) {
                    self.currentUser.cvFile = file;
                }
        })
    }

    private update() {
        if(!this.userForm.valid)return;
        this.loading = true;
        this.userService.update(this.currentUser).subscribe((data)=> {
            this.currentUser.password = this.currentUser.password_repeat = this.loading =  null;
        });
    }

     
}