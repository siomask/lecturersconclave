﻿import {ViewChild, Component, OnInit, TemplateRef, Inject} from '@angular/core';
import {User} from '../../_models/index';
import {AdminService, ConfigService} from '../../_services/index';
import {Config} from '../../_helpers';
import {UserModal, DialogOverviewExampleDialog, UserDialog} from '../../_directives';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

declare var saveAs:any;


@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class UserListComponent implements OnInit {
    private isDesc:boolean = false;
    private modal:boolean = false;
    private model:User = new User();
    private column:string = 'CategoryName';
    private direction:number;
    @ViewChild("userACtion")
    userACtion:UserModal;

    displayedColumns = ['id', 'email', 'firstName', 'lastName', 'createdAt', 'actions'];
    dataSource:MatTableDataSource<any> = new MatTableDataSource([]);
    @ViewChild(MatPaginator) paginator:MatPaginator;
    @ViewChild(MatSort) sort:MatSort;

    constructor(private adminService:AdminService, private confService:ConfigService, private dialog:MatDialog) {
    }

    ngOnInit() {
        setTimeout(()=> {
            this.refreshData();
        })
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    private refreshData() {
        this.adminService.users.forEach((el)=> {
            el.actions = 1;
        })
        this.dataSource = new MatTableDataSource(this.adminService.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    private  openDialog(text, next):void {
        let data:any = {text: text},
            dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
                width: '250px',
                data: data
            });
        dialogRef.afterClosed().subscribe(result => {
            if (data.isOk)next();
        });
    }

    private  openUserDialog(user:any = {}):void {
        let data:any = {
                model: user, onSuccess: (user, next)=> {
                    this.actionUser(user, next);
                }
            },
            dialogRef = this.dialog.open(UserDialog, {
                width: '50%',
                data: data
            });
    }

    private applyFilter(filterValue:string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    private select(user) {
        this.openUserDialog(user);
    }

    private addNewUser() {
        this.openUserDialog();
    }

    private actionUser(user, next) {
        if (user.id) {
            this.adminService.update(user)
                .subscribe(
                    data => {
                        user.password = null;
                        next();
                    },
                    error => {
                    });
        } else {
            this.adminService.create(user)
                .subscribe(
                    data => {

                        if (data.status) {
                            data.data.password = null;
                            this.adminService.users.push(data.data);
                            this.refreshData();
                        }
                        next();
                    },
                    error => {
                    });
        }

    }

    private update(user) {
        this.openDialog("are you sure to update " + user.email, ()=> {
            let _user = {
                id: user.id,
                status: user.status == Config.USER_STATUS.ACTIVE ? Config.USER_STATUS.BAN : Config.USER_STATUS.ACTIVE
            };
            this.adminService.update(_user).subscribe((data)=> {
                if (data.status) {
                    user.status = _user.status;
                }
            });
        });
    }

    private remove(user) {
        this.openDialog("are you sure to drop " + user.email, ()=> {
            this.adminService.delete(user.id).subscribe((data)=> {
                if (data.status) {
                    for (let i = 0; i < this.adminService.users.length; i++) {
                        if (this.adminService.users[i].id == user.id) {
                            this.adminService.users.splice(i, 1);
                            return this.refreshData();
                        }
                    }
                }
            });
        });
    }

    private exportXL() {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Users.xls");
    };

    private  sortF(property) {
        this.isDesc = !this.isDesc; //change the direction
        this.column = property;
        this.direction = this.isDesc ? 1 : -1;
    };
}