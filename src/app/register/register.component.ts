﻿import { ViewChild,OnInit,Component } from '@angular/core';
import { Router } from '@angular/router';
import {FormGroup,FormBuilder,FormControl, FormGroupDirective, NgForm, Validators,ValidatorFn,AbstractControl} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { AlertService, UserService,ConfigService } from '../_services/index';

declare var SimplePay:any;

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control:FormControl | null, form:FormGroupDirective | NgForm | null):boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
export function matchPswValidator(model:any): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const forbidden = model.model.password ==model.model.password_repeat,
            res = {};
        res['password_match']={value: control.value};
        return !forbidden ? res : null;
    };
}
@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html',
    styleUrls: ['index.scss']
})
export class RegisterComponent implements OnInit {
    private year:any = (new Date()).getFullYear();
    model: any = {
        telephone:'+234'
    };
    loading = false;
    private hidePsw = true;
    private els:any={
        status:[
            'Employed',
            'Self Employed',
            'Jobless and searching for job',
            'Jobless and not searching for job',
            'Others'
        ],
        qualifications:[
            'B.Sc.',
            'M.Sc.',
            'BA',
            'MA',
            'PhD',
            'Professional Degrees',
        ]
    };
    private registreForm: FormGroup;
    private matcher = new MyErrorStateMatcher();
    @ViewChild("avatar")avatar:any;
    @ViewChild("cv")CV:any;
    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private confService: ConfigService,
        private formBuilder: FormBuilder) {
        
        this.registreForm = this.formBuilder.group({
            email:    new FormControl('', [
                Validators.required,
                Validators.email,
            ]),
            telephone:    new FormControl('', [
                Validators.required
            ]),
            password:new FormControl('', [
                Validators.required,
                matchPswValidator(this)
            ]),
            firstName:new FormControl('', [
                Validators.required
            ]),
            lastName:new FormControl('', [
                Validators.required
            ]),
            birthday:new FormControl('', []),
            birthCountry:new FormControl('', []),
            profpursuit:new FormControl('', []),
            marritalStatus:new FormControl('', []),
            childrens:new FormControl('', []),
            statusInputed:new FormControl('', []),
            status1:new FormControl('', []),
            interest:new FormControl('', []),
            research:new FormControl('', []),
            profession:new FormControl('', []),
            qualification:new FormControl('', []),
            publisher:new FormControl('', []),
        });
    

    }

    ngOnInit(){
        let self = this;
        this.avatar.nativeElement.addEventListener('change',(e)=>{
            let file = e.target.files[0];
            if (file) {
                let fileReader = new FileReader();
                self.model.avatarFile = file;
                fileReader.onload = function (ev:any) {
                    self.model.avatar = ev.currentTarget.result;
                }
                fileReader.readAsDataURL(file);
            }
        })
        this.CV.nativeElement.addEventListener('change',(e)=>{
            let file = e.target.files[0];
            if (file) {
                self.model.cvFile = file;
            }
        })
    }
    private checkPayment(){
        if(!this.registreForm.valid)return;
        if(this.model.token){
            this.register(this.model.token, true);
        }else{
            var handler = SimplePay.configure({
                token: (token, paid)=>{this.register(
                    token, paid
                )},
                key: this.confService.TOKENS.pay_public
            });
            handler.open(SimplePay.REMEMBER ,{
                email: this.model.email,
                phone: this.model.telephone,
                description: 'Payment for register',
                amount: 100000,
                currency: 'NGN'
            });
        }

    }
    private register(token, paid) {
        if(paid){
            this.loading = true;
            this.model.token = token;
            this.userService.create(this.model,{sp_token:token,sp_paid:paid})
                .subscribe(
                    data => {
                        if(data.status){
                            this.alertService.success('Registration successful,'+data.message, true);
                            this.router.navigate(['']);
                        }else{
                            this.alertService.error('Registration unsuccessful, , '+data.message);
                            this.loading = false;
                        }
                        
                    },
                    error => {
                        this.alertService.error(error);
                        this.loading = false;
                    });
        }

    }
}
